set(HEADER_DIR ${CMAKE_SOURCE_DIR}/include)
set(HEADERS ${HEADER_DIR}/socketcanpp.hpp;)
include_directories(${HEADER_DIR})
add_library(${PROJECT_NAME} SHARED
    Frame.cpp
    Socket.cpp
    RawSocket.cpp)
set_target_properties(${PROJECT_NAME} PROPERTIES VERSION ${socketcanpp_VERSION} SOVERSION ${socketcanpp_VERSION_MAJOR})
install(TARGETS ${PROJECT_NAME} LIBRARY DESTINATION lib)
install(FILES ${HEADERS} DESTINATION include)
