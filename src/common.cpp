#include "socketcanpp.hpp"

using namespace std;
using namespace SocketCANPP;

uint8_t canFdDlcToDlen(uint8_t dlc) {
    uint8_t dlen = 0;

    if (dlc < 9) {
        dlen = dlc;
    } else {
        switch (dlc) {
            case 9:
                dlen = CanFdDlen::DLC9;
                break;
            case 10:
                dlen = CanFdDlen::DLC10;
                break;
            case 11:
                dlen = CanFdDlen::DLC11;
                break;
            case 12:
                dlen = CanFdDlen::DLC12;
                break;
            case 13:
                dlen = CanFdDlen::DLC13;
                break;
            case 14:
                dlen = CanFdDlen::DLC14;
                break;
            case 15:
                dlen = CanFdDlen::DLC15;
                break;
        }
    }

    return dlen;
}

uint8_t canFdDlenToDlc(uint8_t dlen) {
    uint8_t dlc = 0;

    if (dlen < 9) {
        dlc = dlen;
    } else {
        switch (dlen) {
            case CanFdDlen::DLC9:
                dlc = 9;
                break;
            case CanFdDlen::DLC10:
                dlc = 10;
                break;
            case CanFdDlen::DLC11:
                dlc = 11;
                break;
            case CanFdDlen::DLC12:
                dlc = 12;
                break;
            case CanFdDlen::DLC13:
                dlc = 13;
                break;
            case CanFdDlen::DLC14:
                dlc = 14;
                break;
            case CanFdDlen::DLC15:
                dlc = 15;
                break;
        }
    }

    return dlc;
}
