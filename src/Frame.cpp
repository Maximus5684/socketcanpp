#include "socketcanpp.hpp"

using namespace std;
using namespace SocketCANPP;

Frame::Frame(bool isCanFd, uint8_t canFdFlags) {
    _id = 0x0;
    _dlc = isCanFd ? CANFD_MAX_DLC : CAN_MAX_DLC;
    _canFdEnabled = isCanFd;
    _canFdFlags = isCanFd ? canFdFlags : 0x0;
}

Frame::Frame(uint32_t id, uint32_t dlc, std::vector<uint8_t> data, bool isCanFd, uint8_t canFdFlags) {
    if (isCanFd) {
        if (id > CAN_EFF_MASK || dlc > CAN_MAX_DLC || data.size() > CAN_MAX_DLEN) {
            throw system_error(EINVAL, system_category());
        }
    } else {
        if (id > CAN_EFF_MASK || dlc > CANFD_MAX_DLC || data.size() > CANFD_MAX_DLEN) {
            throw system_error(EINVAL, system_category());
        }
    }

    _id = id;
    _dlc = dlc;
    _data = data;
    _canFdEnabled = isCanFd;
    _canFdFlags = canFdFlags;
}

Frame::Frame(can_frame frame) {
    _id = frame.can_id;
    _dlc = frame.can_dlc;

    for (auto dByte : frame.data) {
        _data.push_back(dByte);
    }

    _canFdEnabled = false;
    _canFdFlags = 0x0;
}

Frame::Frame(canfd_frame frame) {
    _id = frame.can_id;
    _dlc = canFdDlenToDlc(frame.len);

    for (auto dByte : frame.data) {
        _data.push_back(dByte);
    }

    _canFdEnabled = true;
    _canFdFlags = frame.flags;
}

Frame::~Frame() {
}

uint32_t Frame::getId() {
    return _id;
}

void Frame::setId(uint32_t id) {
    _id = id;
}

uint8_t Frame::getDlc() {
    return _dlc;
}

bool Frame::setDlc(uint8_t dlc) {
    if (_canFdEnabled) {
        if (dlc > CANFD_MAX_DLC) {
            return false;
        } else {
            if (dlc > CAN_MAX_DLC) {
                switch (dlc) {
                    case CanFdDlen::DLC9:
                    case CanFdDlen::DLC10:
                    case CanFdDlen::DLC11:
                    case CanFdDlen::DLC12:
                    case CanFdDlen::DLC13:
                    case CanFdDlen::DLC14:
                    case CanFdDlen::DLC15:
                        _dlc = dlc;
                        return true;
                        break;
                    default:
                        return false;
                        break;
                }
            } else {
                _dlc = dlc;
                return true;
            }
        }
    } else {
        if (dlc > CAN_MAX_DLC) {
            return false;
        } else {
            _dlc = dlc;
            return true;
        }
    }
}

vector<uint8_t> Frame::getData() {
    return _data;
}

bool Frame::setData(vector<uint8_t> data) {
    size_t dataSize = data.size();

    if (_canFdEnabled) {
        if (dataSize > CANFD_MAX_DLEN) {
            return false;
        } else {
            if (_dlc > CAN_MAX_DLC) {
                switch (_dlc) {
                    case 9: {
                        if (dataSize > CanFdDlen::DLC9) {
                            return false;
                        } else {
                            _data = data;
                            return true;
                        }
                    } break;
                    case 10: {
                        if (dataSize > CanFdDlen::DLC10) {
                            return false;
                        } else {
                            _data = data;
                            return true;
                        }
                    } break;
                    case 11: {
                        if (dataSize > CanFdDlen::DLC11) {
                            return false;
                        } else {
                            _data = data;
                            return true;
                        }
                    } break;
                    case 12: {
                        if (dataSize > CanFdDlen::DLC12) {
                            return false;
                        } else {
                            _data = data;
                            return true;
                        }
                    } break;
                    case 13: {
                        if (dataSize > CanFdDlen::DLC13) {
                            return false;
                        } else {
                            _data = data;
                            return true;
                        }
                    } break;
                    case 14: {
                        if (dataSize > CanFdDlen::DLC14) {
                            return false;
                        } else {
                            _data = data;
                            return true;
                        }
                    } break;
                    case 15: {
                        if (dataSize > CanFdDlen::DLC15) {
                            return false;
                        } else {
                            _data = data;
                            return true;
                        }
                    } break;
                    default:
                        return false;
                        break;
                }
            } else {
                if (dataSize > _dlc) {
                    return false;
                } else {
                    _data = data;
                    return true;
                }
            }
        }
    } else {
        if (dataSize > CAN_MAX_DLEN) {
            return false;
        } else {
            _data = data;
            return true;
        }
    }
}

bool Frame::setData(const uint8_t * data, size_t len) {
    vector<uint8_t> vData;

    for (unsigned int i = 0; i < len; i++) {
        vData.push_back(data[i]);
    }

    return setData(vData);
}

bool Frame::isCanFdEnabled() {
    return _canFdEnabled;
}

void Frame::setCanFd(bool canFd) {
    _canFdEnabled = canFd;
}

uint8_t Frame::getCanFdFlags() {
    return _canFdFlags;
}

void Frame::setCanFdFlags(uint8_t flags) {
    _canFdFlags = flags;
}
