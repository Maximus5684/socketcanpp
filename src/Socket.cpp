#include "socketcanpp.hpp"

using namespace std;
using namespace SocketCANPP;

Socket::Socket(const string& port) :
    _port(port),
    _lastError(SocketError::NONE) {
}

Socket::~Socket() {
}
