#include "socketcanpp.hpp"

using namespace std;
using namespace SocketCANPP;

RawSocket::RawSocket(const string& port, bool isCanFd) :
        Socket(port),
        _errMask(0),
        _canFdEnabled(isCanFd),
        _loopbackEnabled(true),
        _recvOwnMsgsEnabled(false)
{
}

RawSocket::~RawSocket() {
    if (_isConnected) {
        disconnect();
    }
}

/* Returns a bool indicating whether the connection
 * succeeded (true) or failed (false). */
bool RawSocket::connect() {
	_sock = socket(PF_CAN, SOCK_RAW, CAN_RAW);

    if (_sock < 0) {
        _lastError = SocketError::CREATE_FAIL;
        return false;
	}

    strcpy(_ifr.ifr_name, _port.c_str());
    ioctl(_sock, SIOCGIFINDEX, &_ifr);
    _addr.can_family = AF_CAN;
    _addr.can_ifindex = _ifr.ifr_ifindex;

	if (bind(_sock, (struct sockaddr *)&_addr, sizeof(_addr)) < 0) {
        _lastError = SocketError::BIND_FAIL;
        return false;
    }

    if (_canFdEnabled) {
        int canRawFd = 1;
        if (setsockopt(_sock, SOL_CAN_RAW, CAN_RAW_FD_FRAMES, &canRawFd, sizeof(canRawFd)) < 0) {
            _lastError = SocketError::SET_OPT_FAIL;
            return false;
        }
    }

    return true;
}

void RawSocket::disconnect() {
    if (_isConnected) {
        close(_sock);
        _isConnected = false;
    }
}

/* Returns number of bytes written. -1 indicates error. */
int RawSocket::send(Frame& sndFrame) {
    if (_isConnected) {
        ssize_t nBytes;

        if (sndFrame.isCanFdEnabled() && _canFdEnabled) {
            struct canfd_frame frame;

            frame.can_id = sndFrame.getId();
            frame.len = canFdDlcToDlen(sndFrame.getDlc());
            frame.flags = sndFrame.getCanFdFlags();

            vector<uint8_t> vData = sndFrame.getData();

            for (unsigned int i = 0; i < vData.size(); i++) {
                frame.data[i] = vData.data()[i];
            }

            nBytes = write(_sock, &frame, sizeof(frame));

            if (nBytes < 0) {
                _lastError = SocketError::NO_DATA_SENT;
                return -1;
            } else if (nBytes < (ssize_t)sizeof(frame)) {
                _lastError = SocketError::PART_DATA_SENT;
                return -1;
            } else {
                return nBytes;
            }
        } else if (!sndFrame.isCanFdEnabled() && !_canFdEnabled) {
            struct can_frame frame;

            frame.can_id = sndFrame.getId();
            frame.can_dlc = sndFrame.getDlc();
            
            vector<uint8_t> vData = sndFrame.getData();

            for (unsigned int i = 0; i < vData.size(); i++) {
                frame.data[i] = vData.data()[i];
            }

            nBytes = write(_sock, &frame, sizeof(frame));

            if (nBytes < 0) {
                _lastError = SocketError::NO_DATA_SENT;
                return -1;
            } else if (nBytes < (ssize_t)sizeof(frame)) {
                _lastError = SocketError::PART_DATA_SENT;
                return -1;
            } else {
                return nBytes;
            }
        } else {
            return -1;
        }
    } else {
        _lastError = SocketError::NOT_CONNECTED;
        return -1;
    }
}

/* Returns read frame and bytes read. A value of -1 for bytesRead indicates an error. */
Frame RawSocket::receive(int& bytesRead) {
    Frame newFrame;

    if (_isConnected) {
        ssize_t nBytes;

        if (_canFdEnabled) {
            struct canfd_frame frame;

            newFrame.setCanFd(true);
            nBytes = read(_sock, &frame, sizeof(frame));

            if (nBytes < 0) {
                _lastError = SocketError::NO_DATA_RCVD;
                bytesRead = -1;
                return newFrame;
            } else if (nBytes < (ssize_t)sizeof(frame)) {
                _lastError = SocketError::PART_DATA_RCVD;
                bytesRead = -1;
                return newFrame;
            } else {
                bytesRead = nBytes;
                newFrame.setId(frame.can_id);
                newFrame.setDlc(canFdDlenToDlc(frame.len));
                newFrame.setCanFdFlags(frame.flags);
                newFrame.setData(frame.data, frame.len);
            }
        } else {
            struct can_frame frame;

            nBytes = read(_sock, &frame, sizeof(frame));

            if (nBytes < 0) {
                _lastError = SocketError::NO_DATA_RCVD;
                bytesRead = -1;
                return newFrame;
            } else if (nBytes < (ssize_t)sizeof(frame)) {
                _lastError = SocketError::PART_DATA_RCVD;
                bytesRead = -1;
                return newFrame;
            } else {
                bytesRead = nBytes;
                newFrame.setId(frame.can_id);
                newFrame.setDlc(frame.can_dlc);
                newFrame.setData(frame.data, frame.can_dlc);
            }
        }
    } else {
        _lastError = SocketError::NOT_CONNECTED;
        bytesRead = -1;
    }

    return newFrame;
}

void RawSocket::addFilter(uint32_t id, uint32_t mask) {
    if (_isConnected) {
        can_filter cf;
        cf.can_id = id;
        cf.can_mask = mask;
        _filters.push_back(cf);
        if (setsockopt(_sock, SOL_CAN_RAW, CAN_RAW_FILTER, &_filters[0], sizeof(_filters)) < 0) {
            _lastError = SocketError::SET_OPT_FAIL;
        }
    } else {
        _lastError = SocketError::NOT_CONNECTED;
    }
}

/* Returns the number of filters that were removed. */
int RawSocket::removeFilter(uint32_t id, uint32_t mask) {
    int removed = 0;

    if (_isConnected) {
        vector<vector<can_filter>::iterator> toRemove;

        for (auto filt = _filters.begin(); filt != _filters.end(); filt++) {
            if (filt->can_id == id && filt->can_mask == mask) {
                toRemove.push_back(filt);
            }
        }

        if (toRemove.size() > 0) {
            for (auto filt : toRemove) {
                _filters.erase(filt);
                removed++;
            }

            if(setsockopt(_sock, SOL_CAN_RAW, CAN_RAW_FILTER, &_filters[0], sizeof(_filters)) < 0) {
                removed = -1;
                _lastError = SocketError::SET_OPT_FAIL;
            }
        }
    } else {
        _lastError = SocketError::NOT_CONNECTED;
        removed = -1;
    }

    return removed;
}

void RawSocket::clearFilters() {
    if (_isConnected) {
        _filters.clear();
	    if (setsockopt(_sock, SOL_CAN_RAW, CAN_RAW_FILTER, &_filters[0], sizeof(_filters)) < 0) {
            _lastError = SocketError::SET_OPT_FAIL;
        }
    } else {
        _lastError = SocketError::NOT_CONNECTED;
    }
}

void RawSocket::filterAllErrors() {
    if (_isConnected) {
        _errMask = CAN_ERR_MASK;
        setErrorFilter();
    } else {
        _lastError = SocketError::NOT_CONNECTED;
    }
}

/* See linux/can/error.h for possible filter values. */
void RawSocket::filterErrors(uint32_t filter) {
    if (_isConnected) {
        _errMask = filter;
        setErrorFilter();
    } else {
        _lastError = SocketError::NOT_CONNECTED;
    }
}

void RawSocket::clearErrorFilters() {
    if (_isConnected) {
        _errMask = 0;
        setErrorFilter();
    } else {
        _lastError = SocketError::NOT_CONNECTED;
    }
}

void RawSocket::setErrorFilter() {
    if (setsockopt(_sock, SOL_CAN_RAW, CAN_RAW_ERR_FILTER, &_errMask, sizeof(_errMask)) < 0) {
        _lastError = SocketError::SET_OPT_FAIL;
    }
}

void RawSocket::setLoopback(bool enabled) {
    if (_isConnected) {
        int loopback = 0;

        if (enabled) {
            loopback = 1;
            _loopbackEnabled = true;
        } else {
            _loopbackEnabled = false;
        }

        if (setsockopt(_sock, SOL_CAN_RAW, CAN_RAW_LOOPBACK, &loopback, sizeof(loopback)) < 0) {
            _lastError = SocketError::SET_OPT_FAIL;
        }
    } else {
        _lastError = SocketError::NOT_CONNECTED;
    }
}

bool RawSocket::isLoopbackEnabled() {
    return _loopbackEnabled;
}

void RawSocket::setRecvOwnMsgs(bool enabled) {
    if (_isConnected) {
        int recvOwnMsgs = 0;

        if (enabled) {
            recvOwnMsgs = 1;
            _recvOwnMsgsEnabled = true;
        } else {
            _recvOwnMsgsEnabled = false;
        }

        if (setsockopt(_sock, SOL_CAN_RAW, CAN_RAW_RECV_OWN_MSGS, &recvOwnMsgs, sizeof(recvOwnMsgs)) < 0) {
            _lastError = SocketError::SET_OPT_FAIL;
        }
    } else {
        _lastError = SocketError::NOT_CONNECTED;
    }
}

bool RawSocket::isRecvOwnMsgsEnabled() {
    return _recvOwnMsgsEnabled;
}
