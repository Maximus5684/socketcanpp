# SocketCANPP - A C++ Wrapper for Linux SocketCAN #

For copyright information, see COPYING.

## Installation ##
- mkdir build && cd build
- cmake ..
- make
- sudo make install

## Usage ##
Include socketcanpp.hpp in your project. Link against socketcanpp. That's it!

Docs coming soon.

## Tested System Configurations ##
- Ubuntu 14.04.5 (Linux 4.4.0 / gcc 4.9.4)
- Ubuntu 14.10 (Linux 3.16.0 / gcc 4.9.1)
- Ubuntu 15.04 (Linux 3.19.0 / gcc 4.9.2)
- Ubuntu 16.04.1 (Linux 4.4.0 / gcc 5.4.1)
