#ifndef SOCKETCANPP_HPP
#define SOCKETCANPP_HPP

extern "C" {
#include <string.h>
#include <unistd.h>
#include <signal.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/uio.h>
#include <net/if.h>
#include <linux/can.h>
#include <linux/can/raw.h>
#include <linux/can/bcm.h>
}

#include <cstdio>
#include <cstdint>
#include <string>
#include <vector>
#include <system_error>

namespace SocketCANPP {
    enum SocketError {
        NONE,
        CREATE_FAIL,
        BIND_FAIL,
        CONNECT_FAIL,
        SET_OPT_FAIL,
        NOT_CONNECTED,
        WRONG_DATA_TYPE,
        NO_DATA_RCVD,
        PART_DATA_RCVD,
        NO_DATA_SENT,
        PART_DATA_SENT
    };

    enum CanFdDlen {
        DLC9 = 12,
        DLC10 = 16,
        DLC11 = 20,
        DLC12 = 24,
        DLC13 = 32,
        DLC14 = 48,
        DLC15 = 64
    };

    uint8_t canFdDlcToDlen(uint8_t dlen);
    uint8_t canFdDlenToDlc(uint8_t dlc);

    class Socket {
        public:
            Socket(const std::string& port);
            ~Socket();
            virtual bool connect() = 0;
            virtual void disconnect() = 0;
            SocketError getLastError();
        protected:
            std::string _port;
            int _sock;
            bool _isConnected;
            struct ifreq _ifr;
            struct sockaddr_can _addr;
            SocketError _lastError;
    };

    class Frame {
        public:
            Frame(bool isCanFd = false, uint8_t canFdFlags = 0);
            Frame(uint32_t id, uint32_t dlc, std::vector<uint8_t> data, bool isCanFd = false, uint8_t canFdFlags = 0);
            Frame(can_frame frame);
            Frame(canfd_frame frame);
            ~Frame();
            uint32_t getId();
            void setId(uint32_t id);
            uint8_t getDlc();
            bool setDlc(uint8_t dlc);
            std::vector<uint8_t> getData();
            bool setData(std::vector<uint8_t> data);
            bool setData(const uint8_t * data, size_t len);
            bool isCanFdEnabled();
            void setCanFd(bool canFd);
            uint8_t getCanFdFlags();
            void setCanFdFlags(uint8_t);
        private:
            uint32_t _id;
            uint8_t _dlc;
            std::vector<uint8_t> _data;
            bool _canFdEnabled;
            uint8_t _canFdFlags;
    };

    class RawSocket : public Socket {
        public:
            RawSocket(const std::string& port, bool isCanFd = false);
            ~RawSocket();
            bool connect();
            void disconnect();
            int send(Frame& sndFrame);
            Frame receive(int& bytesRead);
            void addFilter(uint32_t id, uint32_t mask);
            int removeFilter(uint32_t id, uint32_t mask);
            void clearFilters();
            std::vector<can_filter> getFilters();
            void filterAllErrors();
            void filterErrors(uint32_t filter);
            void clearErrorFilters();
            uint32_t getErrorFilter();
            bool isCanFdEnabled();
            void setCanFd(bool enabled);
            bool isLoopbackEnabled();
            void setLoopback(bool enabled);
            bool isRecvOwnMsgsEnabled();
            void setRecvOwnMsgs(bool enabled);
        private:
            void setErrorFilter();
            std::vector<can_filter> _filters;
            can_err_mask_t _errMask;
            bool _canFdEnabled;
            bool _loopbackEnabled;
            bool _recvOwnMsgsEnabled;
    };

    /*class BCMSocket : public Socket { //TODO
        public:
            BCMSocket(const std::string& port);
            ~BCMSocket();
        private:
    };*/

    /*class BCMMessage : public Socket { //TODO
        public:
            BCMMessage();
            ~BCMMessage();
            bool createCycTransmission(uint32_t canId, std::vector<Frame> frames, long repeatIntervalUs, uint32_t flags = 0);
            bool deleteCycTransmission(uint32_t canId);
            bool sendTxRead(uint32_t canId, std::vector<Frame> frames, uint32_t flags = 0);
            bool sendTxSend(uint32_t canId, Frame frame, uint32_t flags = 0);
    };*/
}

#endif //SOCKETCANPP_HPP
